const createEvent = async (body) => {
  return {
    headers: {},
    body: typeof body === "undefined" ? "" : JSON.stringify(body),
  };
};

module.exports = createEvent;
