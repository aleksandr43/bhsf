const Joi = require("joi");

const schema = Joi.object({
  scalar: Joi.number().integer().greater(0).required(),
  array: Joi.array().items(Joi.number().integer().required()),
});

module.exports = schema;
