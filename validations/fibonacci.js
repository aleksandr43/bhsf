const Joi = require("joi");

const schema = Joi.object({
  number: Joi.number().integer().greater(0).required(),
});

module.exports = schema;
