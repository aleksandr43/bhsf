const generateResponseOnError = async (errors) => {
  const { details } = errors;

  return {
    statusCode: 400,
    body: {
      message: details,
    },
  };
};

module.exports = generateResponseOnError;
