const Joi = require("joi");

const schema = Joi.object({
  scalar: Joi.number().integer().greater(0).required(),
  vector: Joi.array()
    .items(Joi.array().items(Joi.number().required()))
    .required(),
});

module.exports = schema;
