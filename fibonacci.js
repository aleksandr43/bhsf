const generateResponseOnError = require("./validations/messageOnErrors");
const schema = require("./validations/fibonacci");

const fibonacci = async (n) => {
  if (n <= 1) {
    return n;
  }

  return (await fibonacci(n - 1)) + (await fibonacci(n - 2));
};

const handler = async (event, context) => {
  const body = JSON.parse(event.body);
  let errors;
  try {
    await schema.validateAsync(body);
  } catch (e) {
    errors = e;
  }

  if (errors?.details?.length >= 1) {
    return await generateResponseOnError(errors);
  }

  return {
    statusCode: 200,
    body: await fibonacci(body.number),
  };
};

module.exports = {
  fibonacci,
  handler,
};
