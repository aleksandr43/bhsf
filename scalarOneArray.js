/* 2. Create similar function to calculate product between an scalar and one array. e.g. Array: [1, 10, 7, 9, 17, 29] x 2 Result: [2, 20, 14, 18, 34, 58]. */

const schema = require("./validations/scalarOneArray");
const generateResponseOnError = require("./validations/messageOnErrors");

/**
 *
 * @param {number} scalar
 * @param {Array<number>} array
 * @returns
 */
const calculateScalarOnArray = async (scalar, array) => {
  return array.map((v) => v * scalar);
};

const handler = async (event, context) => {
  const body = JSON.parse(event.body);
  const { scalar, array } = body;

  let errors;
  try {
    await schema.validateAsync(body);
  } catch (error) {
    errors = error;
  }

  if (errors?.details?.length >= 1) {
    return await generateResponseOnError(errors);
  }

  return {
    statusCode: 200,
    body: await calculateScalarOnArray(scalar, array),
  };
};

module.exports = {
  calculateScalarOnArray,
  handler,
};
