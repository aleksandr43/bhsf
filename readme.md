Hi,

Welcome to a small concept checking for NodeJS.

Below you can see an example. Our code, is made in NodeJS for AWS Serverless Function, aka Lambdas. Usually, a lambda has one method named handler, and has two parameters, `event` and `context`. The arg `event` has the request information, such as `queryParameters`, `pathParameters`, `headers` and `body`. 

For our execercise, please review how those fields come inside the event (in the example you can see `headers` and `body`).


```
const handler = (event, context) => {
  const body = JSON.parse(event.body);
  const { firstName, lastName } = body;
  return {
    statusCode: 200,
    body:`Hi ${firstName} ${lastName} from NodeJS`
  };
}

const event = {
  headers: {},
  body: JSON.stringify({
    firstName: "John",
    lastName: "Doe"
  })
};

const response = handler(event);
console.log('Response is ', response)
```

Result: Response is: {statusCode: 200, body: 'Hi John Doe from NodeJS'}.

Excersises:

1. Create a similar function to calculate the Fibonnaci Number. The number to be calculated should come inside the JSON body and the response should be similar to the previous example JSON with `statusCode` and `body`.

2. Create similar function to calculate product between an scalar and one array. e.g. Array: [1, 10, 7, 9, 17, 29] x 2 Result: [2, 20, 14, 18, 34, 58].



3. Create a similar function to calculate product between an scalar and one vector e.g: Vector
```
     | 2 | 2 |   | 4 | 4 |
 2 * | 5 | -1| = |10 |-2 |
     | 0 | 10|   | 0 |20 |
```
For this calc, please use Promises and divide the solution, one Promise per row to calcululate it. At then end merge the results to have the result.


For all the scenarios, please prepare a set of scenarios. If you have time, please use jest to create the Unit Test with the previous scenarios.

You can use: https://replit.com/languages/nodejs to run your code and verify the results.