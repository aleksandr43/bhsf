/* Create a similar function to calculate product between an scalar and one vector e.g: Vector */

const { calculateScalarOnArray } = require("./scalarOneArray");
const generateResponseOnError = require("./validations/messageOnErrors");
const schema = require("./validations/scalarOneVector");
/**
 *
 * @param {number} scalar
 * @param {Array<Array<number>>} vector
 */
const scalarVector = async (scalar, vector) => {
  let calculated = [];
  for (let index = 0; index < vector.length; index++) {
    calculated[index] = await calculateScalarOnArray(scalar, vector[index]);
  }
  return calculated;
};

const handler = async (event, context) => {
  const body = JSON.parse(event.body);
  const { scalar, vector } = body;
  let errors;
  try {
    await schema.validateAsync(body);
  } catch (error) {
    errors = error;
  }
  if (errors?.details?.length >= 1) {
    return await generateResponseOnError(errors);
  }

  return {
    statusCode: 200,
    body: scalarVector(scalar, vector),
  };
};

module.exports = {
  scalarVector,
  handler,
};
