const createEvent = require("./event");
const { scalarVector, handler } = require("./scalarOneVector");

describe("scalarOneVector Suitecase", () => {
  describe("unit test", () => {
    it("should calculate a new vector by the scalar", async () => {
      const result = await scalarVector(2, [
        [2, 2],
        [5, -1],
        [0, 10],
      ]);
      expect(result).toStrictEqual([
        [4, 4],
        [10, -2],
        [0, 20],
      ]);
    });
  });
  describe("integration test", () => {
    it("returns status code 400 when input is invalid/malformed", async () => {
      let invalidInput = await createEvent({});
      let result = await handler(invalidInput, {});

      expect(result.statusCode).toBe(400);
    });

    it("returns status code 200 when input is valid", async () => {
      const input = await createEvent({
        scalar: 5,
        vector: [
          [2, 4, 6],
          [8, 10, 12],
        ],
      });

      const result = await handler(input, {});

      expect(result.statusCode).toBe(200);
    });
  });
});
