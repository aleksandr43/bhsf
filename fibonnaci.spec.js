const createEvent = require("./event");
const { fibonacci, handler } = require("./fibonacci");

describe("Fibonacci suite case", () => {
  describe("unit testing", () => {
    it("should calculate fibonacci of the input", async () => {
      const result = await fibonacci(5);
      expect(result).toBe(5);
    });
  });

  describe("integration test", () => {
    it("return status code 400 when input is invalid", async () => {
      let invalidInput = await createEvent({ number: false });
      let result = await handler(invalidInput, {});

      expect(result.statusCode).toBe(400);

      invalidInput = await createEvent({ number: "Something" });
      result = await handler(invalidInput, {});

      expect(result.statusCode).toBe(400);

      invalidInput = await createEvent({ number: {} });
      result = await handler(invalidInput, {});

      expect(result.statusCode).toBe(400);

      invalidInput = await createEvent({});
      result = await handler(invalidInput, {});

      expect(result.statusCode).toBe(400);
    });

    it("return status code 200 with the computed number", async () => {
      let input = await createEvent({ number: 1 });
      let result = await handler(input, {});

      expect(result.statusCode).toBe(200);
      expect(result.body).toBe(1);
    });
  });
});
