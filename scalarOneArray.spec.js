const createEvent = require("./event");
const { calculateScalarOnArray, handler } = require("./scalarOneArray");

describe("scalarOneArray Suitcase", () => {
  describe("unit test", () => {
    it("calculate new array based on the scalar", async () => {
      const result = await calculateScalarOnArray(2, [1, 10, 7, 9, 17, 29]);
      expect(result).toStrictEqual([2, 20, 14, 18, 34, 58]);
    });
  });

  describe("integration test", () => {
    it("returns status code 400 when the input is invalid", async () => {
      let invalidInput = await createEvent({});
      let result = await handler(invalidInput, {});

      expect(result.statusCode).toBe(400);
    });

    it("returns status code 200 when input is valid", async () => {
      const input = await createEvent({ scalar: 2, array: [2, 7, 9, 10] });
      const result = await handler(input, {});

      expect(result.statusCode).toBe(200);
      expect(result.body).toStrictEqual([4, 14, 18, 20]);
    });
  });
});
